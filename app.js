'use strict'

// let request = require('request');
// const URL = 'http://api.hubapi.com/content/api/v2/templates?hapikey=a7bd632a-6b09-4bf5-8cec-5bf7e919193b';
//
// request().get(URL, (res) => {
//   let body = '';
//   res.setEncoding('utf8');
//
//   res.on('data', (chunk) => {
//       body += chunk;
//       console.log(chunk.);
//   });
//
//   res.on('end', (res) => {
//       // res = JSON.parse(body);
//       // console.log(res);
//   });
// }).on('error', (e) => {
//   console.log(e.message); //エラー時
// });


var request = require("request");
const fs = require("fs")

function getTemplateList (){
  request.get({
    url: "http://api.hubapi.com/content/api/v2/templates",
    qs: {
      hapikey: "a7bd632a-6b09-4bf5-8cec-5bf7e919193b",
      limit: 2000
    }
  }, function (error, response, body) {
    let result = JSON.parse(body)

    for (let item of result.objects) {
      if (/Ark/.test(item.label)) {
        console.log(`${item.template_type}: ${item.label}: ${item.id}`);
      }
    }
  });
}

// getTemplateList()

function getTemplateById (){
  request.get({
    url: "http://api.hubapi.com/content/api/v2/templates/8782822607",
    qs: {
      hapikey: "a7bd632a-6b09-4bf5-8cec-5bf7e919193b"
    }
  }, function (error, response, body) {
    let result = JSON.parse(body)

      console.dir(`${body}`);
  });
}
// getTemplateById()
