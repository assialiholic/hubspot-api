var request = require('request')
const fs = require('fs')

const importRequest = {
  name: 'test',
  files: [
    {
      fileName: `test.csv`,
      dateFormat: 'DAY_MONTH_YEAR',
      fileImportPage: {
        hasHeader: true,
        columnMappings: [
          {
            ignored: false,
            columnName: 'First Name',
            idColumnType: null,
            propertyName: 'firstname',
            foreignKeyType: null,
            columnObjectTypeId: '0-1',
            associationIdentifierColumn: false,
          },
          {
            ignored: false,
            columnName: 'Last Name',
            idColumnType: null,
            propertyName: 'lasttname',
            foreignKeyType: null,
            columnObjectTypeId: '0-1',
            associationIdentifierColumn: false,
          },
          {
            ignored: false,
            columnName: 'Email',
            idColumnType: 'HUBSPOT_ALTERNATE_ID',
            propertyName: 'email',
            foreignKeyType: null,
            columnObjectTypeId: '0-1',
            associationIdentifierColumn: false,
          }
        ],
      },
    },
  ],
}

const importFilePath = `./test.csv`
const importFileReadStream = fs.createReadStream(importFilePath)

var options = {
  method: 'POST',
  url: 'https://api.hubapi.com/crm/v3/imports/',
  qs: { hapikey: '78113a00-279f-4967-995b-1bbc63134a0a' },
  headers: { accept: 'application/json' },
  body: JSON.stringify(importRequest),
  json: true,
  formData: {
    myFile: fs.createReadStream('./test.csv'),
  },
}

// fs.createReadStream('contacts.csv').pipe(
//   request(options, (error, response, body) => {
//     if (error) throw new Error(error)

//     console.log(body)
//   }),
// )

request(options, (error, response, body) => {
  if (error) throw new Error(error)

  console.log(body)
})
