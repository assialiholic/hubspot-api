const request = require("request");

function updateContact (){
  // Update Contact By Emailへのリクエストボディを予め定義
  const contactObj = {
    "properties": [
      {
        "property": "firstname",
        "value": "Atsushi"
      },
      {
        "property": "lastname",
        "value": "Handa"
      },
      {
        "property": "privacy_policy_test",
        "value": false
      },
      {
        "property": "muching_visit_date",
        "value": Date.parse('2019-11-01') // 日付形式をUNIX時間に変換。変換後の値は1572566400000
      },
      {
        "property": "hs_content_membership_notes",
        "value": "パンセスタッフ\n職種：テクニカルディレクター、フロントエンドエンジニアなど"
      },
      {
        "property": "followercount",
        "value": 1000
      },
      {
        "property": "hs_language",
        "value": "ja-jp"
      },
      {
        "property": "interesting_hubspot_tools",
        "value": "crm;marketing_hub;sales_hub"
      },
    ]
  }

  // requestモジュールのオプションを予め用意
  const options = {
    method: 'POST',
    url: `http://api.hubapi.com/contacts/v1/contact/email/handa@pensees.co.jp/profile`,
    qs: {
      hapikey: "6b9d9010-1fd9-4852-b8f9-0bb0125d5d84"
    },
    json: true, // bodyをJSON形式にするだけでなく、Content-Typeにapplication/jsonを設定する役割も果たす
    body: contactObj
  }

  request(options, (error, response, body) => {
    // エラーがあればErrorオブジェクトを投げ、それ以降の処理を停止
    if (error) throw new Error(error);

    // エラーが無ければ、レスポンスのステータスコードを表示
    console.dir(response.statusCode)
  });
}

updateContact()
