// requestモジュールの読み込み
const request = require("request");
// fsモジュールの読み込み（.jsonファイルの出力に使用）
const fs = require("fs")

function getDeals (email){
  // requestモジュールのオプションを予め用意
  const options = {
    method: 'GET',
    url: `https://api.hubapi.com/contacts/v1/contact/email/${email}/profile`,
    qs: {
      hapikey: '6b9d9010-1fd9-4852-b8f9-0bb0125d5d84',
    }
  }

  // HubSpot APIへのリクエスト実行
  request(options, function (error, response, body) {
    // エラーがあればErrorオブジェクトを投げ、それ以降の処理を停止
    if (error) throw new Error(error);

    // エラーが無ければ、レスポンスボディをcontact.jsonとして保存
    const result = JSON.parse(body)
    fs.writeFileSync('./contact.json', JSON.stringify(result, null, 2))
  });
}

// 関数の実行
getDeals('handa@pensees.co.jp')
