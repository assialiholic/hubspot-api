var request = require("request");

function postPage (pageId){
  const pageObj = require('./pages.json');

    const baseUrl = "http://api.hubapi.com/content/api/v2/pages"
    const options = {
      url: pageId ? `${baseUrl}/${pageId}` : baseUrl,
      qs: {
        hapikey: "af0d9c4b-151d-49be-841a-933d956ccb3b" // demo portal for clients
      },
      headers: {
          'Content-Type':'application/json'
      },
      json: true,
      body: pageObj
    }

    request.put(options, function (error, response, body) {
      try {
        console.log(`done`)
      } catch(error) {
        console.log(response)
        throw new Error(error)
      }
    });
}

postPage(10417592120)
