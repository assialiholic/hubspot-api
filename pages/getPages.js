var request = require("request");
const fs = require("fs")

function getPages (pageId){
  const baseUrl = "http://api.hubapi.com/content/api/v2/pages"
  const options = {
    url: pageId ? `${baseUrl}/${pageId}` : baseUrl,
    qs: {
      hapikey: "6b9d9010-1fd9-4852-b8f9-0bb0125d5d84"
    }
  }

  request.get(options, function (error, response, body) {
    const result = JSON.parse(body)
    let output

    if (Array.isArray(result.objects)) {
      const array = []
      result.objects.forEach(item => {
        const obj = {}
        obj.footer_html = item.meta.footer_html
        obj.head_html = item.meta.head_html
        obj.is_draft = item.is_draft
        obj.meta_description = item.meta_description
        obj.name = item.name
        obj.password = item.meta.password
        obj.publish_date = item.publish_date
        obj.publish_immediately = item.publish_immediately
        obj.slug = item.slug
        obj.subcategory = item.subcategory
        obj.widget_containers = item.widget_containers
        obj.widgets = item.widgets
        obj.template_path = item.template_path
        array.push(obj)
      });
      output = array
    } else {
      const obj = {}
      obj.footer_html = result.meta.footer_html
      obj.head_html = result.meta.head_html
      obj.is_draft = result.is_draft
      obj.meta_description = result.meta_description
      obj.name = result.name
      obj.password = result.meta.password
      obj.publish_date = result.publish_date
      obj.publish_immediately = result.publish_immediately
      obj.slug = result.slug
      obj.subcategory = result.subcategory
      obj.widget_containers = result.widget_containers
      obj.widgets = result.widgets
      obj.template_path = result.template_path
      output = obj
    }
    fs.writeFileSync('./pages.json', JSON.stringify(output, null, '  '))
  });
}
getPages(10368249522)
