var request = require("request");

function postPages (){
  var pages = require('./pages.json');

  pages.forEach((item, index) => {
    setTimeout(() => {
      createPost(item)
    }, 1000);
  });

  function createPost(obj) {
    request.post({
      url: "http://api.hubapi.com/content/api/v2/pages",
      qs: {
        hapikey: "af0d9c4b-151d-49be-841a-933d956ccb3b" // demo portal for clients
      },
      headers: {
          'Content-Type':'application/json'
      },
      json: true,
      body: obj
    }, function (error, response, body) {
      try {
        console.log(`${obj.name} done`)
      } catch(error) {
        console.log(response)
        throw new Error(error)
      }
    });
  }
}

postPages()
