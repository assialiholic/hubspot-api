var request = require("request");

function createBlogAuthors (){
  var authors = require('./blogAuthors.json');

  authors.objects.forEach((item, index) => {
    setTimeout(() => {
      create(item)
    }, 1000);
  });


  function create(obj) {
    request.post({
      url: "http://api.hubapi.com/blogs/v3/blog-authors",
      qs: {
        hapikey: "API_KEY_HERE"
      },
      headers: {
          'Content-Type':'application/json'
      },
      json: true,
      body: obj
    }, function (error, response, body) {
      try {
        console.log(obj.username)
      } catch(error) {
        console.log(response)
        throw new Error(error)
      }
    });
  }
}

createBlogAuthors()
