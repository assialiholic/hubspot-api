var request = require("request");
const fs = require("fs")

function getTopics (){
  request.get({
    url: "http://api.hubapi.com/blogs/v3/topics",
    qs: {
      hapikey: "API_KEY_HERE",
      limit: 100
    }
  }, function (error, response, body) {
    const result = JSON.parse(body)
    fs.writeFileSync('topics.json', JSON.stringify(result, null, '  '))
  });
}
getTopics()
