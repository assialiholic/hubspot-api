var request = require("request");
const fs = require("fs")

function getBlogAuthors (){
  request.get({
    url: "http://api.hubapi.com/blogs/v3/blog-authors",
    qs: {
      hapikey: "API_KEY_HERE"
    }
  }, function (error, response, body) {
    const result = JSON.parse(body)
    fs.writeFileSync('blogAuthors.json', JSON.stringify(result, null, '  '))
  });
}
getBlogAuthors()
