var request = require("request");
const fs = require("fs")

function getBlogPosts (){
  request.get({
    url: "http://api.hubapi.com/content/api/v2/blog-posts",
    qs: {
      hapikey: "17285f6b-0958-46ca-9c78-2a019a27fce4",
      limit: 300,
      offset: 300,
      content_group_id: "35819483961"
    }
  }, function (error, response, body) {
    const result = JSON.parse(body)
    const array = []

    result.objects.forEach(item => {
      let obj = {}
      obj.id = item.id
      // obj.blog_author_id = item.blog_author_id
      // obj.campaign = item.campaign
      // obj.campaign_name = item.campaign_name
      // obj.featured_image = item.featured_image
      // obj.footer_html = item.footer_html
      // obj.head_html = item.head_html
      // obj.keywords = item.keywords
      // obj.meta_description = item.meta_description
      // obj.name = item.name
      obj.post_body = item.post_body
      obj.post_summary = item.post_summary
      // obj.publish_date = item.publish_date
      // obj.publish_immediately = JSON.stringify(item.publish_immediately)
      // obj.slug = item.slug
      // obj.topic_ids = item.topic_ids
      // obj.use_featured_image = item.use_featured_image
      // obj.widgets = item.widgets
      // obj.featured_image = item.featured_image
      // obj.content_group_id = 35819483961
      array.push(obj)
    });
    fs.writeFileSync('data.json', JSON.stringify(array, null, '  '))
  });
}
getBlogPosts()
