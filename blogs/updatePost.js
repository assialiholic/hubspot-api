
var request = require("request");


function callAPI(item) {
  request.put({
    url: `https://api.hubapi.com/content/api/v2/blog-posts/${item.id}`,
    qs: {
      hapikey: "17285f6b-0958-46ca-9c78-2a019a27fce4"
    },
    headers: {
        'Content-Type':'application/json'
    },
    json: true,
    body: item
  }, function (error, response, body) {
    try {
      console.log(item.id)
    } catch(error) {
      console.log(response)
      throw new Error(error)
    }
  });
}


let counter = 0;
const posts = require('./data-merged.json');
console.log(posts.length)

function updatePosts(postObj){
  if (counter === posts.length) return false;
  counter++;

  callAPI(postObj);

  setTimeout(() => {
    updatePosts(posts[counter])
  }, 1000);
}

updatePosts(posts[0])