var request = require("request");

function postBlogPosts (){
  var posts = require('./data.json');

  posts.forEach((item, index) => {
    setTimeout(() => {
      createPost(item)
    }, 500);
  });

  function createPost(obj) {
    request.post({
      url: "http://api.hubapi.com/content/api/v2/blog-posts",
      qs: {
        hapikey: "API_KEY_HERE"
      },
      headers: {
          'Content-Type':'application/json'
      },
      json: true,
      body: obj
    }, function (error, response, body) {
      try {
        console.log(obj.name)
      } catch(error) {
        console.log(response)
        throw new Error(error)
      }
    });
  }
}

postBlogPosts()
