const fetch = require("node-fetch");
function callback() {
  console.log('hi')
}

const apiKey = "abf657b5-d745-4efd-8993-15a13c6f46c7";
const tableId = 1058410;
const tableApiUrl = `https://api.hubapi.com/hubdb/api/v2/tables/${tableId}`

fetch(`${tableApiUrl}/rows?hapikey=${apiKey}`, {
  method: "GET"
})
  .then(res => {
    return res.json();
  })
  .then(result => {
    const targetRow = result.objects.filter(obj => {
      return obj.values[1] === "2019/08/17のセミナー 午前";
    })[0];

    targetRow.values[2] += 1;

    fetch(`${tableApiUrl}/rows/${targetRow.id}?hapikey=${apiKey}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(targetRow)
    })
      .then(() => {
        console.log('update done');

        fetch(`${tableApiUrl}/publish?hapikey=${apiKey}`, {
          method: "PUT"
        })
        .then(() => {
          console.log('publish done')
          callback(null, {updatedRow: inputData.seminar_name});
        })
      })
      .catch(callback);
  })
  .catch(callback);
