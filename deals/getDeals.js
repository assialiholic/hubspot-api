var request = require("request");
const fs = require("fs")
const piplineId = {
  sales: {
    id: 'default',
    stages: {
      not_complete: '7a881750-783b-4b39-bd86-797b65f56cd0', // 協業希望（未成立）
      not_process: 'ccdd85df-9ed3-4dba-80e7-534fbf0f31a6' // 協業希望（未成立）
    }
  },
  partner: {
    id: 642487,
    stages: {
      not_complete: 665301
    }
  }
}

function getDeals (){
  request.get({
    url: "http://api.hubapi.com/deals/v1/deal/paged?hapikey=6b9d9010-1fd9-4852-b8f9-0bb0125d5d84&properties=dealname&properties=dealstage&properties=pipeline&limit=250"
  }, function (error, response, body) {
    const result = JSON.parse(body)
    const array = []

    result.deals.forEach(item => {
      if (
        item.properties.pipeline.value === piplineId.sales.id
        &&
        item.properties.dealstage.value === piplineId.sales.stages.not_complete
        ) {
        array.push(item)
      }
    });
    fs.writeFileSync('./deals.json', JSON.stringify(result, null, '  '))
  });
}
getDeals()
