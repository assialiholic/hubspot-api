var request = require("request");
const piplineId = {
  partner: {
    id: 642487,
    stages: {
      not_complete: 665301, //未成立
      not_process: 642493 //未処理
    }
  }
}

const whichPipline = piplineId.partner.id
const whichStage = piplineId.partner.stages.not_process


function updateDeals (){
  var deals = require('./data/deals.json');

  deals.forEach((item, index) => {
    setTimeout(() => {
      postData(item.dealId, {
        "properties": [
          {
            "name": "pipeline",
            "value": whichPipline
          },
          {
            "name": "dealstage",
            "value": whichStage
          }
        ]
      })
    }, 1000);
  });

  function postData(id, obj) {
    request.put({
      url: `http://api.hubapi.com/deals/v1/deal/${id}`,
      qs: {
        hapikey: "6b9d9010-1fd9-4852-b8f9-0bb0125d5d84"
      },
      headers: {
          'Content-Type':'application/json'
      },
      json: true,
      body: obj
    }, function (error, response, body) {
      try {
        console.log(body)
      } catch(error) {
        console.log('eroor')
        console.log(response)
        throw new Error(error)
      }
    });
  }
}

updateDeals()
