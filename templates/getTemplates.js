var request = require("request");
const fs = require("fs")

function getTemplates (){
  request.get({
    url: "http://api.hubapi.com/content/api/v2/templates",
    qs: {
      hapikey: "6b9d9010-1fd9-4852-b8f9-0bb0125d5d84",
      limit: 150
    }
  }, function (error, response, body) {
    const result = JSON.parse(body)
    const array = []

    result.objects.forEach(item => {
      let obj = {}
      obj.category_id = item.category_id
      obj.folder = item.folder
      obj.template_type = item.template_type
      obj.path = item.path
      obj.source = item.source
      obj.is_available_for_new_content = item.is_available_for_new_content
      array.push(obj)
    });
    fs.writeFileSync('templates.json', JSON.stringify(array, null, '  '))
  });
}
getTemplates()
