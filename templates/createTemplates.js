var request = require("request");

function postTemplates (){
  var templates = require('./templates.json');

  templates.forEach((item, index) => {
    setTimeout(() => {
      createPost(item)
    }, 1000);
  });

  function createPost(obj) {
    request.post({
      url: "http://api.hubapi.com/content/api/v2/templates",
      qs: {
        hapikey: "af0d9c4b-151d-49be-841a-933d956ccb3b" // demo portal for clients
      },
      headers: {
          'Content-Type':'application/json'
      },
      json: true,
      body: obj
    }, function (error, response, body) {
      try {
        console.log(obj.path)
      } catch(error) {
        console.log(response)
        throw new Error(error)
      }
    });
  }
}

postTemplates()
